var exec = require("cordova/exec");
var PLUGIN_NAME = "DatagranSDK";

module.exports = {
	identify: function(userId, successCallback, errorCallback) {
	    cordova.exec(successCallback, errorCallback, PLUGIN_NAME, "identify", [userId]);
    },

    resetDGuserid: function(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, PLUGIN_NAME, "resetDGuserid", []);
    },

    trackCustom: function(params, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, PLUGIN_NAME, "trackCustom", [params]);
    },

	updateGeoParam: function(addGeo, successCallback, errorCallback) {
	    cordova.exec(successCallback, errorCallback, PLUGIN_NAME, "updateGeoParam", [addGeo]);
    }
};