package io.datagran.cordova.sdk.android;

import android.app.Activity;
import android.content.Context;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import io.datagran.sdk.android.Tracker;

public class DatagranSDK extends CordovaPlugin  {

    private static boolean initiated = false;
    private static boolean addGeo = false;

    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        System.out.println("Data initialize::");
        Activity context = this.cordova.getActivity();
        init(context);
    }

    public void init(Activity context) {
        Boolean addGeo = true;
        Tracker tracker = null;
        String datagranAppKey = preferences.getString("DatagranAppKey", "");
        String datagranWorkspaceId = preferences.getString("DatagranWorkspaceId", "");
        if(!datagranAppKey.isEmpty() && !datagranWorkspaceId.isEmpty()) {
            int datagranBatchValue = preferences.getInteger("DatagranBatchValue", 0);
            int datagranWaitResponse = preferences.getInteger("DatagranWaitResponse", 0);
            if (datagranBatchValue != 0 && datagranWaitResponse != 0) {
                tracker = Tracker.init(context, addGeo, datagranAppKey, datagranWorkspaceId, datagranBatchValue, datagranWaitResponse);
            } else if (datagranBatchValue > 0 && datagranWaitResponse == 0) {
                tracker = Tracker.init(context, addGeo, datagranAppKey, datagranWorkspaceId, datagranBatchValue);
            } else {
                tracker = Tracker.init(context, addGeo, datagranWorkspaceId, datagranAppKey);
            }
            System.out.println("tracker::" + tracker.toString());
            Tracker.singleton().setActivityContext(context);
            initiated = true;
        }
    }

    @Override
    public boolean execute(String action, JSONArray args,
                           CallbackContext callbackContext) {
        Activity context = this.cordova.getActivity();
        System.out.println("action::"+action);

        if(!initiated) {
            callbackContext.error("Datagran SDK was not initialized with appKey and WorkspaceID. Please setup the keys and restart the application.");
            return false;
        }

        if(action.equals("identify")) {
            cordova.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        String userId = args.getString(0);
                        if(!userId.isEmpty()) {
                            Tracker.singleton().identify(userId, context, addGeo);
                            callbackContext.success("Identify method successfully invoked !!!");
                        } else {
                            callbackContext.error("Invalid parameters");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            return true;
        } else if(action.equals("resetDGuserid")) {
            cordova.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Tracker.singleton().resetDGuserid();
                    callbackContext.success("resetDGuserid method successfully invoked !!!");
                }
            });
            return true;
        } else if(action.equals("trackCustom")) {
            cordova.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        JSONObject json = args.getJSONObject(0);
                        if(json != null && json.length() >= 1 && null != json.getString("eventName")) {
                            String eventName = json.getString("eventName");
                            Gson gson = new Gson();
                            JsonElement jsonElement = gson.fromJson(json.toString(), JsonElement.class);
                            JsonObject payload = gson.fromJson((gson.toJson(jsonElement)), JsonObject.class);
                            payload.remove("eventName");
                            Tracker.singleton().trackCustom(eventName, payload, context, addGeo);
                            callbackContext.success(eventName +" event has been successfully passed !!!");
                        } else {
                            callbackContext.error("Invalid parameters");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        callbackContext.error("Exception "+e.getMessage());
                    }
                }
            });
            return true;
        } else if(action.equals("updateGeoParam")) {
            cordova.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        String geoParam = args.getString(0);
                        if(!geoParam.isEmpty()) {
                            if(geoParam.equals("on"))
                                addGeo = true;
                            else
                                addGeo = false;
                            callbackContext.success("updateGeoParam method successfully invoked !!!");
                        } else {
                            callbackContext.error("Invalid parameters");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            return true;
        } else {
            callbackContext.error("Invalid action");
        }

        return false; // Returning false results in a "MethodNotFound" error.
    }

}